FROM alpine:latest
ENV IS_DOCKER=1

RUN set -eux;\
  apk add --no-cache \
  bash bash-completion tree sudo

RUN set -eux;\
  addgroup -g 1001 alpine; \
  adduser -h /home/alpine -s /bin/bash -G alpine -D -u 1001 alpine

# allow sudo
# https://github.com/sudo-project/sudo/issues/42
# https://bugzilla.redhat.com/show_bug.cgi?id=1773148
RUN echo "Set disable_coredump false" >> /etc/sudo.conf

USER 1001
COPY files/.profile /home/alpine/
COPY files/.bashrc /home/alpine/

ENV HOME /home/alpine
WORKDIR /home/alpine

ENTRYPOINT ["/bin/bash"]
